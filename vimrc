"Instructions 
"  .vimrc -> /home/deploy/.vim/vimrc
"  :BundleInstall      -> run it in the comand line
set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" My Bundles here:
"
" original repos on github
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
Bundle 'tpope/vim-rails.git'
Bundle 'scrooloose/nerdtree'
" vim-scripts repos
Bundle 'L9'
Bundle 'FuzzyFinder'
" non github repos
Bundle 'altercation/vim-colors-solarized'
Bundle 'git://git.wincent.com/command-t.git'
" ...

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..
" 

set nocompatible                " choose no compatibility with legacy vi
set encoding=utf-8              " sensible encoding
set showcmd                     " display incomplete commands
set number                      " need those line numbers
set ruler                       " show the line/column number of the cursor position
set shell=sh                    " hack for rvm
"" Whitespace
set nowrap                      " wrap lines, switch with set wrap/nowrap
set linebreak                   " break line for wrapping at end of a word
set tabstop=2 shiftwidth=2      " a tab is two spaces
set expandtab                   " use spaces
set backspace=indent,eol,start  " backspace through everything in insert mode
set scrolloff=3                 " Minimum number of screen lines to keep above/below the cursor

set history=10000
set undolevels=1000
set nobackup
set hlsearch                    " highlight matches
set incsearch                   " incremental searching
set ignorecase                  " searches are case insensitive...
set smartcase                   " ... unless they contain at least one capital letter
set wildmenu                    " enhanced command line completion
set wildignore+=*.o,*.obj,.bundle,coverage,.DS_Store,_html,.git,*.rbc,*.class,.svn,vendor/gems/*,vendor/rails/*
set complete=.,w,b,u,t          " don't complete with included files (i)
set foldmethod=manual           " for super fast autocomplete
"searches use normal regexes
nnoremap / /\v
vnoremap / /\v
" Colors
set term=xterm-256color
syntax enable
set t_Co=16
set background=light
" Window
set cmdheight=2                 " number of lines for the command line
set laststatus=2                " always have a status line
set statusline=%<%f\ (%{&ft})\ %-4(%m%)%=%-19(%3l,%02c%03V%)
set showtabline=2               " always show tab bar
set winwidth=84                 " 

" " Mappings
let mapleader=","               " use , as leader instead of backslash

" Splits
" open tests in new split
map <leader>v :vs<cr>,.

" quick split and jump into window
map :vs :vsplit<cr><c-l>

" easier navigation between split windows
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l



" Remap shift key failure
command! W :w
command! Wq :wq
command! E :e

" Retain indent when pasting code
nnoremap <leader>pt :set invpaste paste?<CR>
set pastetoggle=<leader>pt
set showmode

" use OS clipboard
"set clipboard=unnamed

" force vim
map <Left> :echo "damnit!"<cr>
map <Right> :echo "you suck!"<cr>
map <Up> :echo "this is why you fail"<cr>
map <Down> :echo "nooooo!"<cr>

" Plugin mappings
" Fugutive shortcuts
map :gs :Gstatus<cr>
map :gb :Gblame<cr>
map :gd :Gdiff<cr>

"  Ack
map <leader>/ :Ack<space>


" Run
map <leader>r :!ruby % -v<cr>



" Resize windows quickly
" reset with <c-w>=
nmap <leader>H :vertical res +20<cr>
nmap <leader>L :vertical res -20<cr>
nmap <leader>fef ggVG=
nmap <leader>n :NERDTreeToggle<CR>


let g:solarized_termcolors=256
syntax enable
set background=light
colorscheme solarized
